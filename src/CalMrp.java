import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class CalMrp implements CanCalculateMRP{
    private int lt,ss,min,lotSize,onHandOld,max,pass;
    private int level;
    private ArrayList<Integer> gross = new ArrayList<>();

    private ArrayList<Integer> netArr = new ArrayList<>();
    private List<Integer> onHand = new CopyOnWriteArrayList<>();



    String name;
    ArrayList<Integer> scheduled = new ArrayList<>();
    List<Integer> planned = new CopyOnWriteArrayList<>();

    List<PlanOrder> planOrderList = new ArrayList<>();

    CalMrp(int lt, int ss, int min, int lotSize, int onHandOld, int pass, int max, String name){

        this.name =name;
        this.max=max;
        this.lt=lt;
        this.ss=ss;
        this.min=min;
        this.onHandOld=onHandOld;
        this.pass=pass;
        this.lotSize =lotSize;


    }
    public void setData(int gross , int scheduled){
        this.gross.add(gross);
        this.scheduled.add(scheduled);
    }
    public void setDataScheduled(int scheduled){
        this.scheduled.add(scheduled);
    }
    public void setDataGross(int gross){
        this.gross.add(gross*pass);
    }
    public void getData(){

        System.out.println("Node : "+ name +" Lv. "+level+"\n"+"Safety Stock: "+ss+" LT : "+lt +" No.User/Unit: "+pass  + " Lot-Size : "+lotSize + " Min : "+ min+ " Max : " + max);
        System.out.println("Gross     "+ gross);
        System.out.println("Scheduled "+ scheduled);
        System.out.println("OnHand    "+onHand);
        System.out.println("Net       "+ netArr);
        System.out.println("Planned   "+planned);
    }


    public void getPlanned(){
        int indexPlan=0;

        SimpleDateFormat sdf = new SimpleDateFormat("E yyyy-MMM-dd");
        Calendar calendar = Calendar.getInstance();
        Date date ;

        for(int plan: planned){
            //indexPlan++;
            calendar.add(Calendar.DATE, 1);
            date =  calendar.getTime();
            if(sdf.format(date).contains("Sat"))
            {
                calendar.add(Calendar.DATE, 1);
                date =  calendar.getTime();
            }
            if(sdf.format(date).contains("Sun"))
            {
                calendar.add(Calendar.DATE, 1);
                date =  calendar.getTime();
            }
            if(plan!=0){
                PlanOrder planOrder = new PlanOrder();
                planOrder.setPartNumber(name);
                planOrder.setPlanOrderDate(date);
                planOrder.setPlanQuantity((double) plan);
                planOrderList.add(planOrder);
            }
        }


    }

    public void calculateMrp(int level){
        this.level = level;
        int handProblem ;
        int net;
        int indexOnHand=-1;
        int plan;
        try{
           firstDataOnHand();
            for(int onHandProblem: onHand){
                indexOnHand++;
                if(onHandProblem<ss){
                    net = (onHand.get(indexOnHand-1)+scheduled.get(indexOnHand))-gross.get(indexOnHand)-ss;

                    if(net<0){
                            netArr.add(net);
                            plan=Math.abs(net);
                            scheduled.remove(indexOnHand);
                            scheduled.add(indexOnHand,checkMin(checkLot(plan)));
                        try {
                            planned.add(indexOnHand-lt,checkMin(checkLot(plan)));
                        }catch (Exception e){
                            System.out.println("Node "+ name+" แผนนี้ใช้ไม่ได้ สั่งไม่ทัน  ***********************"+e);
                        }

                    }else {
                        planned.add(indexOnHand-lt,0);
                        netArr.add(0);
                    }
                    handProblem=(onHand.get(indexOnHand-1)+scheduled.get(indexOnHand))-gross.get(indexOnHand);
                    onHand.remove(indexOnHand);
                    onHand.add(indexOnHand,handProblem);
                }else{
                    netArr.add(0);
                    planned.add(0);
                }
            }
        }catch (Exception e){
            System.out.println("Node "+ name + " : ต้องเพิ่ม ของเก่าในคลัง *****************************" +e);
        }
        setMax();
        }

    private void setMax(){
        boolean stop = true;
        for (int plan: planned){
            if(plan>max && max >0){
                stop = false;
                break;
            }
        }

        calculateNewPlan(stop);
        calculateNewScheduled();

            onHand.clear();

        calculateNewDataOnHand();
    }
    private void calculateNewPlan(Boolean stop){

        int TheOneMaxIndex = 0;
        int newPlanOutLoop=0;
        int dataMax=max;
        try {
            while (!stop){
                int difference = 0 ;
                stop =true;

                for(int newPlanInLoop: planned) {
                    if(newPlanInLoop > dataMax){
                        stop =false;
                        TheOneMaxIndex = planned.indexOf(newPlanInLoop);
                        difference = newPlanInLoop-dataMax;
                        newPlanOutLoop=dataMax;
                        break;
                    }
                }
                planned.remove(TheOneMaxIndex);
                planned.add(TheOneMaxIndex,checkMin(checkLot(newPlanOutLoop)));

                    int beforeOverMax = planned.get(TheOneMaxIndex-1)+ difference;//ค่าของก่อนตำแหน่ง TheOneMax
                    planned.remove(TheOneMaxIndex-1);//ลบตัวก่อนหน้า
                    planned.add(TheOneMaxIndex-1,checkMin(checkLot(beforeOverMax)));

            }
        }catch (Exception e){
            System.out.println("Node "+ name+" ต้องเพิ่ม Max limit *********************** ["+e+"]");
        }
    }
    private void calculateNewScheduled(){
        int mLt=lt;


        for(int newPlan: planned){
            scheduled.add(mLt++,newPlan);
        }

        while (true){
            scheduled.remove(planned.size());
            if(scheduled.size()==planned.size()){
                break;
            }
        }

    }
    private void calculateNewDataOnHand(){
        int newOnHandOld;
        int newOnHand;
try {
    newOnHandOld = (onHandOld + scheduled.get(0))-gross.get(0);
    onHand.add(Math.abs(newOnHandOld));
    for(int addOnHand =1; addOnHand < gross.size();addOnHand++){
        newOnHand = (onHand.get(addOnHand-1)+scheduled.get(addOnHand))-gross.get(addOnHand);
        onHand.add(Math.abs(newOnHand));
    }
}catch (Exception e){
    System.out.println("Node "+ name + " : ต้องเพิ่ม ของเก่าในคลัง *****************************" +e);
}
    }
    private void firstDataOnHand(){
        int newOnHandOld;
        int newOnHand;
        try {
            newOnHandOld = (onHandOld + scheduled.get(0))-gross.get(0);
            onHand.add(newOnHandOld);
            for(int addOnHand =1; addOnHand < gross.size();addOnHand++){
                newOnHand = (onHand.get(addOnHand-1)+scheduled.get(addOnHand))-gross.get(addOnHand);
                onHand.add(newOnHand);
            }
        }catch (Exception e){
            System.out.println("Node "+ name + " : ต้องเพิ่ม ของเก่าในคลัง *****************************" +e);
        }
            }


    private int checkMin(int plan){
        if(plan<min){
            plan = min;
        }
        return checkLot(plan);
    }
    private  int checkLot(int plan){
        int mLot ;
        if(plan%lotSize!=0){
            mLot = (plan+lotSize)/lotSize;
            plan = (int) (Math.ceil(mLot)*lotSize);
        }
        return plan;
    }



    public void calDataMrp(int gross, int scheduled){

//        ArrayList<Integer> grossArray = new ArrayList<>();
//        ArrayList<Integer> scheduledArray = new ArrayList<>();
//        ArrayList<Integer> netArr = new ArrayList<>();
//        grossArray.add(gross*pass);
//        scheduledArray.add(scheduled);
//
//        this.planned.add(0);
//        int     lastGrossArray = grossArray.get(grossArray.size()-1),
//                lastScheduledArray = scheduledArray.get(scheduledArray.size()-1);
//        int minS=0,plan;
//
//        onHandOld = (onHandOld+lastScheduledArray)- lastGrossArray;
//        try {
//            if(onHandOld<ss ){
//                onHandOld = (onHand.get(onHand.size()-1)+lastScheduledArray)-lastGrossArray-ss;
//                if(min==0){
//                    plan = Math.abs(onHandOld);
//
//                }else {
//                    while (min<Math.abs(onHandOld)){//lotSize
//                        min+=lotSize;
//                        minS+=lotSize;//ตัวว่าเพิ่มขึ้นเท่าไหร
//                    }
//                    plan=min;
//                    min -=minS;//หักออก เพราะ ค่า min จะเปลี่ยนแปลงไม่ได้ เพื่อใช้ในตัวทัดไป
//                }
//                this.planned.remove(onHand.size()-lt);
//                this.planned.add(onHand.size()-lt,plan);
//
//                scheduledArray.remove(lastScheduledArray);
//                scheduledArray.add(plan);
//                netArr.add(onHandOld);
//
//                onHandOld = ((onHand.get(onHand.size()-1)+plan))-lastGrossArray;
//                onHand.add(onHandOld);
//
//            }else {
//                this.onHand.add(onHandOld);
//                netArr.add(0);
//            }
//        }catch (Exception e){
//            System.out.println(e);
//            System.out.println( name+" "+"แผนนี้ใช้ไม่ได้");
//        }
//
//        for(int i : grossArray){
//            gross.add(i);
//        }
//        for(int i : scheduledArray){
//            scheduled.add(i);
//        }
//
//        for(int i : netArr){
//            this.netArr.add(i);
//        }


    }//Code เก่า

    @Override
    public List<PlanOrder> calculateMRP() {
        return null;
    }
}
