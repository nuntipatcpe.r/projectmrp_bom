import java.util.List;

public interface CanCalculateMRP {
    List<PlanOrder> calculateMRP();
}
