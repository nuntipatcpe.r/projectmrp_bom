import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class FullProjectMrp implements CanCalculateMRP{
    private List<Nodes> bom = new ArrayList<>();
    private List<CalMrp> mrp = new ArrayList<>();
    List<PlanOrder> planList = new ArrayList<>();
    PlanOrder plan = new PlanOrder();

    FullProjectMrp(){
//        dataMrpReal();
        mrpAndLevelTest();
//        mrpOneNodeTest();
//        bomTableRadWhite();
//        bomHasNull();
//        bomNotFoundNull();

    }

    FullProjectMrp(List<PlanOrder> planList){
        ///this.planList = planList;
    }
    private void searchLevel() {

        addFirstNodeCaseNotFoundNull();

        try {
            calculateAllNodeLevel();
        }catch (Exception e){
            System.out.println("ของในมือไม่พอ");
        }

        try {
            Collections.sort(bom, new Comparator<Nodes>() {
                @Override
                public int compare(Nodes o1, Nodes o2) {
                    return o1.nodeData.compareToIgnoreCase(o2.nodeData);
                }
            });
        }catch (Exception e){
            System.out.println("sort Name node error :"+e);
        }// sort Name node
        keepTheMaxNode();
        try {
            Collections.sort(bom, new Comparator<Nodes>() {
                @Override
                public int compare(Nodes o1, Nodes o2) {
                    return o1.levelInteger.compareTo(o2.levelInteger);
                }
            });
        }catch (Exception e){
            System.out.println("sort Lv. error :"+e);
        } // sort Lv.

    }
    private void calculateAllNodeLevel(){
        List<Nodes> lastLevel = new CopyOnWriteArrayList<>();
        Integer level = 0;
        boolean stop = false;
        while (!stop) {
            stop = true;

            if(level>= bom.size()){
                stop = true;
                break;
            } // Protect loop Infinity

            if (level == 0) {
                for (Nodes parentNodes : bom) {
                    if (parentNodes.parentNodeData == null) {
                        parentNodes.setLevelInteger(0);
                        lastLevel.add(parentNodes);

//                        MRP*************************************

                        for(CalMrp d : mrp){
                            if(d.name== parentNodes.nodeData){
                                d.calculateMrp(level);
                            }
                        }
//                        MRP*************************************
                    }
                }
            }else {

                for (Nodes nextNodes : bom) {
                    if (nextNodes.levelInteger == null) {
                        for (Nodes lastNodes : lastLevel) {
                            if (nextNodes.parentNodeData == lastNodes.nodeData) {
                                nextNodes.setLevelInteger(level);
//                                MRP*************************************

                                for(CalMrp nextNodeMrp : mrp){
                                    if(nextNodeMrp.name== nextNodes.nodeData){//check node ที่เข้ามา

                                        for(CalMrp lastMrp : mrp) {//เทียบ node กับ  lastNodeArray
                                            if(lastMrp.name== lastNodes.nodeData){

                                                for(int i =0; i<nextNodeMrp.scheduled.size();i++){
                                                    nextNodeMrp.setDataGross(lastMrp.planned.get(i));
                                                }
                                                //nextNodeMrp.level=level;
                                                nextNodeMrp.calculateMrp(level);

                                            }
                                        }
                                    }
                                }
//                                MRP*************************************
                                break;
                            }
                        }

                    }
                }
            }

            lastLevel = new CopyOnWriteArrayList<>();
//            lastLevel = new ArrayList<>();
            for (Nodes checkLv : bom) {
                if (checkLv.levelInteger == level) {
                    lastLevel.add(checkLv);
                }
            }

//            Reduce Redundancy Data
            for (Nodes nodes : lastLevel) {
                for (Nodes pNodes : lastLevel) {
                    if( pNodes.parentNodeData == nodes.nodeData){
                        lastLevel.remove(pNodes);
                        pNodes.setLevelInteger(null);
                    }
                }
            }

//            งงให้เปิด  ********************
//            for(Node lLevelTest : lastLevel ){
//                System.out.println(lLevelTest.nodeData + " "+ lLevelTest.parentNodeData +" Lv. "+lLevelTest.levelInteger);
//            }
//            System.out.println("_____");

//            Reduce Redundancy Data


            level++;
            for (Nodes out : bom) {
                if (out.levelInteger == null) {
                    stop = false;
                    break;
                }
            }
        }
    }
    private void addFirstNodeCaseNotFoundNull(){

        List<String> nameNodeNotFoundNull = new CopyOnWriteArrayList<>();//จำๆๆๆๆๆๆๆๆๆๆ

        for(Nodes addNameParent : bom){
            if(addNameParent.parentNodeData !=null){
                nameNodeNotFoundNull.add(addNameParent.parentNodeData);
            }
        }

        for(String name: nameNodeNotFoundNull){
            for(Nodes nodes : bom){
                if(nodes.nodeData ==name){
                    nameNodeNotFoundNull.remove(name);
                }
            }
        }
//            Reduce Redundancy Data
        Set<String> reduceRedundancy = new HashSet<>(nameNodeNotFoundNull);//จำๆๆๆๆๆๆๆๆๆๆ
        nameNodeNotFoundNull.clear();
        nameNodeNotFoundNull.addAll(reduceRedundancy);
//            Reduce Redundancy Data

//        System.out.println("Parent Node Add: "+nameNodeNotFoundNull);
        for (String createFistNode: nameNodeNotFoundNull){
            Nodes firstNodes = new Nodes(createFistNode, null);
            bom.add(firstNodes);
        }

    }
    private void keepTheMaxNode(){
        boolean stop;
        stop=false;
        while (!stop){
            try {
                stop=true;
                for (int i = 0; i < bom.size(); i++) {
                    if (bom.get(i).nodeData == bom.get(i+1).nodeData) {
                        if(bom.get(i).levelInteger>bom.get(i+1).levelInteger){
                            bom.remove(i+1);
                        }else{
                            bom.remove(i);
                        }
                        stop = false;
                    }
                }
            }catch (Exception e){

            }
        }
    }

    private void show() {
        for (Nodes a : bom) {
            System.out.println(a.nodeData + "  " + a.parentNodeData + " Lv. " + a.levelInteger + " " );
        }
        System.out.println();
    }

    private void mrpAndLevelTest(){
        //**************************************************** ADD MRP *********************************************************
        CalMrp bicycle = new CalMrp(1,25,200,25,100,1,0,"Bicycle");
        bicycle.setData(100,450);
        bicycle.setData(325,0);
        bicycle.setData(275,450);
        bicycle.setData(200,0);
        bicycle.setData(300,0);
        bicycle.setData(175,0);
        bicycle.setData(350,0);
        bicycle.setData(250,0);

        CalMrp frame = new CalMrp(1,25,200,50,0,2,250,"Frame");
        frame.setDataScheduled(450);
        frame.setDataScheduled(0);
        frame.setDataScheduled(450);
        frame.setDataScheduled(0);
        frame.setDataScheduled(0);
        frame.setDataScheduled(0);
        frame.setDataScheduled(0);
        frame.setDataScheduled(0);

        CalMrp wheel = new CalMrp(1,25,200,50,500,2,500,"Wheel");
        wheel.setDataScheduled(450);
        wheel.setDataScheduled(0);
        wheel.setDataScheduled(450);
        wheel.setDataScheduled(0);
        wheel.setDataScheduled(0);
        wheel.setDataScheduled(0);
        wheel.setDataScheduled(0);
        wheel.setDataScheduled(0);

        CalMrp rim = new CalMrp(1,25,200,50,0,1,1000,"Rim");
        rim.setDataScheduled(450);
        rim.setDataScheduled(0);
        rim.setDataScheduled(450);
        rim.setDataScheduled(0);
        rim.setDataScheduled(0);
        rim.setDataScheduled(0);
        rim.setDataScheduled(0);
        rim.setDataScheduled(0);

        mrp.add(bicycle);
        mrp.add(frame);
        mrp.add(wheel);
        mrp.add(rim);
//**************************************************** ADD Level Bom ***************************************************
        Nodes bicycleNodes = new Nodes(bicycle.name, null);
        Nodes frameNodes = new Nodes(frame.name, bicycle.name);
        Nodes wheelNodes = new Nodes(wheel.name, bicycle.name);
        Nodes rimNodes = new Nodes(rim.name, frame.name);

        bom.add(bicycleNodes);
        bom.add(frameNodes);
        bom.add(wheelNodes);
        bom.add(rimNodes);
//**************************************************** Show Level Bom **************************************************
        searchLevel();
//        show();
//**************************************************** Show Cal MRP ****************************************************


        for(CalMrp i : mrp){
//           i.getData();
            i.getPlanned();
            for(PlanOrder a : i.planOrderList){
                planList.add(a);
            }
//            planList.add(i.planOrder);
//            planList.add(i.planOrderList);
            //System.out.println("___________________________________________");

        }
//        for(PlanOrder a : planList){
//            System.out.println("___"+a.getPartNumber() + " " + a.getPlanQuantity());
//        }



    }
    private void mrpOneNodeTest(){

        CalMrp bicycle = new CalMrp(3,25,200,25,100,1,250,"Bicycle");
        bicycle.setData(125,450);
        bicycle.setData(325,0);
        bicycle.setData(275,450);
        bicycle.setData(200,0);
        bicycle.setData(300,0);
        bicycle.setData(175,0);
        bicycle.setData(350,0);
        bicycle.setData(250,0);
        bicycle.calculateMrp(0);
        bicycle.getData();
        bicycle.getPlanned();



    }
    private void bomTableRadWhite(){
        Nodes tableWhite = new Nodes("tableWhite", null);//0
        Nodes tableRad = new Nodes("tableRad", null);//0

        Nodes tableLegsWhite = new Nodes("tableLegsWhite",        "tableWhite");//1
        Nodes tableRadWhiteRad = new Nodes("tableRadWhiteRad",      "tableRad");//1
        Nodes tableLegsRad = new Nodes("tableLegsRad",          "tableRad");//1
        Nodes whiteWoodenBoard = new Nodes("whiteWoodenBoard",      "tableWhite");//2
        Nodes whiteWoodenBoard_2 = new Nodes("whiteWoodenBoard",      "tableRadWhiteRad");//2
        Nodes woodenNut = new Nodes("woodenNut",             "tableLegsRad");//2
        Nodes woodenNut_2 = new Nodes("woodenNut",             "tableLegsWhite");//2

        Nodes red = new Nodes("red",                   "tableLegsRad");//2
        Nodes red_2 = new Nodes("red",                   "tableRadWhiteRad");//2

        Nodes woodenPlanks = new Nodes("woodenPlanks",          "whiteWoodenBoard");
        Nodes woodenPlanks_2 = new Nodes("woodenPlanks",          "whiteWoodenBoard");


        Nodes woodenPlanksLittle = new Nodes("woodenPlanksLittle",    "woodenNut");
        Nodes woodenPlanksLittle_2 = new Nodes("woodenPlanksLittle",    "woodenNut");

        Nodes nut = new Nodes("nut",                   "woodenNut");
        Nodes nut_2 = new Nodes("nut",                   "woodenNut");

        Nodes white = new Nodes("white",                 "whiteWoodenBoard");
        Nodes white_3 = new Nodes("white",                 "whiteWoodenBoard");

        Nodes white_2 = new Nodes("white",                 "tableLegsWhite");
        Nodes white_4 = new Nodes("white",                 "woodenPlanks");

//        bom.add(tableWhite);
//        bom.add(tableRad);

        bom.add(tableLegsWhite);
        bom.add(tableRadWhiteRad);
        bom.add(tableLegsRad);

        bom.add(whiteWoodenBoard);
        bom.add(whiteWoodenBoard_2);

        bom.add(woodenNut);
        bom.add(woodenNut_2);
        bom.add(red);
        bom.add(red_2);

        bom.add(woodenPlanks);
        bom.add(woodenPlanks_2);

        bom.add(woodenPlanksLittle);
        bom.add(woodenPlanksLittle_2);

        bom.add(nut);
        bom.add(nut_2);


        bom.add(white);
        bom.add(white_2);


        bom.add(white_3);
        bom.add(white_4);

        searchLevel();
        show();
    }
    private void bomHasNull(){
        Nodes bicycleNodes = new Nodes("bicycle", null);
        Nodes bicycleNodes_2 = new Nodes("bicycle2", null);
        Nodes frameNodes = new Nodes("frame", "bicycle");
        Nodes wheelNodes = new Nodes("wheel", "bicycle");
        Nodes rimNodes = new Nodes("rim", "frame");
        Nodes rimNodes2 = new Nodes("rim2", "bicycle3");

        bom.add(bicycleNodes);
        bom.add(bicycleNodes_2);
        bom.add(frameNodes);
        bom.add(wheelNodes);
        bom.add(rimNodes);
        bom.add(rimNodes2);

        searchLevel();
        show();

    }
    private void bomNotFoundNull(){
        Nodes data1 = new Nodes("A2001","A1001");
        Nodes data2 = new Nodes("A2002","A1001");
        Nodes data3 = new Nodes("A2003","A1001");
        Nodes data4 = new Nodes("A2004","A1001");
        Nodes data5 = new Nodes("A2005","A1001");
        Nodes data6 = new Nodes("A2006","A1006");
        Nodes data7 = new Nodes("A2007","A1007");
        Nodes data8 = new Nodes("A2008","A1008");
        Nodes data9 = new Nodes("A2009","A1009");
        Nodes data10 = new Nodes("A2001","A1010");
        Nodes data11 = new Nodes("B2001","B1001");
        Nodes data12 = new Nodes("B2002","B1002");
        Nodes data13 = new Nodes("B2003","B1003");
        Nodes data14 = new Nodes("B2004","B1004");
        Nodes data15 = new Nodes("B2005","B1005");
        Nodes data16 = new Nodes("C2001","C1001");
        Nodes data17 = new Nodes("C2002","C1002");
        Nodes data18 = new Nodes("C2003","C1003");
        Nodes data19 = new Nodes("C2004","C1004");
        Nodes data20 = new Nodes("C2005","C1005");

        bom.add(data1);
        bom.add(data2);
        bom.add(data3);
        bom.add(data4);
        bom.add(data5);
        bom.add(data6);
        bom.add(data7);
        bom.add(data8);
        bom.add(data9);
        bom.add(data10);
        bom.add(data11);
        bom.add(data12);
        bom.add(data13);
        bom.add(data14);
        bom.add(data15);
        bom.add(data16);
        bom.add(data17);
        bom.add(data18);
        bom.add(data19);
        bom.add(data20);
        searchLevel();
        show();


    }

    private void dataMrpReal(){

//        CalculateMrp a1001 = new CalculateMrp(5,200,100,100,80,1,2000,"A1001");
//        CalculateMrp a1002 = new CalculateMrp(7,200,100,100,80,1,2000,"A1002");
//        CalculateMrp a1003 = new CalculateMrp(7,250,100,100,80,1,2000,"A1003");
//        CalculateMrp a1004 = new CalculateMrp(7,500,100,100,80,1,2000,"A1004");
//        CalculateMrp a1005 = new CalculateMrp(10,500,100,100,80,1,2000,"A1005");
//        CalculateMrp a1006 = new CalculateMrp(5,1000,100,100,80,1,2000,"A1006");
//        CalculateMrp a1007 = new CalculateMrp(7,1000,100,100,80,1,2000,"A1007");
//        CalculateMrp a1008 = new CalculateMrp(7,1000,100,100,80,1,2000,"A1008");
//        CalculateMrp a1009 = new CalculateMrp(7,2000,100,100,80,1,2000,"A1009");
//        CalculateMrp a1010 = new CalculateMrp(10,5000,100,100,80,1,2000,"A1010");
//        a1001.setData(125,450);
//
//        mrp.add(a1001);
//
//
//
//
//        Node bicycleNode = new Node(bicycle.name, null);
//        bom.add(bicycleNode);
//
//        calLevelEdit();
//        show();
//
//        for(CalculateMrp i : mrp){
//            i.getData();
//            i.getPlanned();
//            System.out.println("___________________________________________");
//        }
    }

    public List<PlanOrder> getPlanList() {
        return planList;
    }

    public void setPlanList(List<PlanOrder> planList) {
       this.planList = planList;

//        for (PlanOrder i: this.planList){
//            System.out.println(i.getPartNumber()+" "+ i.getPlanQuantity() );
//        }
    }

    @Override
    public
    List<PlanOrder> calculateMRP() {
        return getPlanList();
    }
}
