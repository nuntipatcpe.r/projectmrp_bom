import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Main {

    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException {

        CanCalculateMRP cals =  new FullProjectMrp();

        SimpleDateFormat sdf = new SimpleDateFormat("E dd-MMM-yy");
        Date dayNow = new Date();
        System.out.println();
        System.out.println("Start date : "+sdf.format(dayNow));
        System.out.println("________________________________");

        for(PlanOrder i : cals.calculateMRP() ){
            System.out.println("You have to find "+i.getPartNumber() +" quantity!! "
                    +i.getPlanQuantity() + " by date "
                    + sdf.format(i.getPlanOrderDate()));
        }
        System.out.println("________________________________");

//       ReadFileXML readFileXML = new ReadFileXML();

    }
}
