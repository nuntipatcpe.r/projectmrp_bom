import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class PlanOrder {
    private Date planOrderDate;
    private String partNumber;
    private Double planQuantity;

    public PlanOrder(){
        this.planOrderDate = new Date();
        this.partNumber = "";
        this.planQuantity = 0.0;
    }

    public Date getPlanOrderDate() {
        return planOrderDate;
    }

    public void setPlanOrderDate(Date planOrderDate) {
        this.planOrderDate = planOrderDate;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public Double getPlanQuantity() {
        return planQuantity;
    }

    public void setPlanQuantity(Double planQuantity) {
        this.planQuantity = planQuantity;
    }

}
