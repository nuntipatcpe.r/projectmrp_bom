import java.util.*;

public class TestBom {
    List<Nodes> bom = new ArrayList<>();

    int countLoop =0;
    int MyCountLoop =0;
    TestBom() {
    }
    public void setBom() {
//        Node bicycle = new Node("bicycle", null);//0
//        Node wheel = new Node("wheel", "bicycle");//1
//        Node frame = new Node("frame", "bicycle");//1
//        Node rim = new Node("rim", "wheel");//2
//        Node tire = new Node("tire", "wheel");//2
//        Node spokes = new Node("spokes", "wheel");//2
//        Node color = new Node("color", "frame");//2
//        Node tireValve = new Node("tireValve", "tire");//
//        Node bolt = new Node("bolt", "tireValve");
//        Node nut = new Node("nut", "tireValve");
//        Node cap = new Node("cap", "tireValve");
//        bom.add(bicycle);
//        bom.add(bicycle);
//        bom.add(wheel);
//        bom.add(frame);
//        bom.add(rim);
//        bom.add(tire);
//        bom.add(spokes);
//        bom.add(color);
//        bom.add(tireValve);
//        bom.add(bolt);
//        bom.add(nut);
//        bom.add(cap);

        Nodes tableWhite = new Nodes("tableWhite", null);//0
        Nodes tableRad = new Nodes("tableRad", null);//0

        Nodes tableLegsWhite = new Nodes("tableLegsWhite",        "tableWhite");//1
        Nodes tableRadWhiteRad = new Nodes("tableRadWhiteRad",      "tableRad");//1
        Nodes tableLegsRad = new Nodes("tableLegsRad",          "tableRad");//1
        Nodes whiteWoodenBoard = new Nodes("whiteWoodenBoard",      "tableWhite");//2
        Nodes whiteWoodenBoard_2 = new Nodes("whiteWoodenBoard",      "tableRadWhiteRad");//2
        Nodes woodenNut = new Nodes("woodenNut",             "tableLegsRad");//2
        Nodes woodenNut_2 = new Nodes("woodenNut",             "tableLegsWhite");//2

        Nodes red = new Nodes("red",                   "tableLegsRad");//2
        Nodes red_2 = new Nodes("red",                   "tableRadWhiteRad");//2

        Nodes woodenPlanks = new Nodes("woodenPlanks",          "whiteWoodenBoard");
        Nodes woodenPlanks_2 = new Nodes("woodenPlanks",          "whiteWoodenBoard");


        Nodes woodenPlanksLittle = new Nodes("woodenPlanksLittle",    "woodenNut");
        Nodes woodenPlanksLittle_2 = new Nodes("woodenPlanksLittle",    "woodenNut");

        Nodes nut = new Nodes("nut",                   "woodenNut");
        Nodes nut_2 = new Nodes("nut",                   "woodenNut");

        Nodes white = new Nodes("white",                 "whiteWoodenBoard");
        Nodes white_3 = new Nodes("white",                 "whiteWoodenBoard");

        Nodes white_2 = new Nodes("white",                 "tableLegsWhite");
        Nodes white_4 = new Nodes("nut",                 "woodenPlanks");

        bom.add(tableWhite);
        bom.add(tableRad);
        bom.add(tableLegsWhite);
        bom.add(tableRadWhiteRad);
        bom.add(tableLegsRad);

        bom.add(whiteWoodenBoard);
        bom.add(whiteWoodenBoard_2);

        bom.add(woodenNut);
        bom.add(woodenNut_2);
        bom.add(red);
        bom.add(red_2);

        bom.add(woodenPlanks);
        bom.add(woodenPlanks_2);

        bom.add(woodenPlanksLittle);
        bom.add(woodenPlanksLittle_2);

        bom.add(nut);
        bom.add(nut_2);


        bom.add(white);
        bom.add(white_2);
        bom.add(white_3);
        bom.add(white_4);



    }

    public void calLevelEdit() {
        ArrayList<Nodes> lastLevel = new ArrayList<>();
        Integer level = 0;
        Integer indexRepeatLevel=0;
        boolean stop = false;

        Collections.sort(bom, new Comparator<Nodes>() {
            @Override
            public int compare(Nodes o1, Nodes o2) {
                return o1.nodeData.compareToIgnoreCase(o2.nodeData);
            }
        });

        //วนลูปหาLevel ถ้าไม่มีตัวไหน เป็น Null ให้ เพิ่มจนกว่า ทุกตัวจะไม่ใช่ null
        //!stop  ค่าเริ่ทต้นเป็น false ให้กลับค่า มาเข้าLoop

        while (!stop) {
            stop = true;
            if (level == 0) {
                //loop bom หา parentNode == null => level ==0;
                for (Nodes parentNodes : bom) {
                    if (parentNodes.parentNodeData == null) {
                        parentNodes.setLevelInteger(0);
                        lastLevel.add(parentNodes);
                    }else
                    {
                        for(Nodes nodes : bom) {
                            if(parentNodes.levelInteger==null){
                                continue;
                            }
                            if(parentNodes.parentNodeData != nodes.nodeData && parentNodes.levelInteger!=0) {
                                nodes.setLevelInteger(level);
                                lastLevel.add(nodes);
                                break;
                            }
                        }
                    }
                }
            }else {
                for (Nodes nextNodes : bom) {
                    ///countLoop++;
                    //for(bom) หาตัวไหน == null{จะมีทุกตัวยกเวนตัวที่เพิ่มไปแล้ว}
                    if (nextNodes.levelInteger == null) {
                        //for(lastLevel) node ที่เคยเพิ่มไปก่อนหน้านี้
                        for (Nodes lNodes : lastLevel) {
                            //   countLoop++;
                            //ถ้า node.parentNodeArr ไหน == node ที่วน for(lastLevel) ออกมา ถ้ามีให้Add level
                            if (nextNodes.parentNodeData == lNodes.nodeData) {
                                nextNodes.setLevelInteger(level);
                                break;
                            }
                        }
                    }
                }
            }
            // ไม่จำเป็นต้อง clear loop ก็ได้ แต่ถ้ามี ข้อมูลเยอะๆ จะประหยัดเนื้อที่
            lastLevel = new ArrayList<>();
            //หาทุกตัวใน bom ถ้าเจอ level เท่ากับ level รอบนั้นๆ ให้ Add ข้อมูลไป ใน Array lastLevel
            for (Nodes lLv : bom) {
                if (lLv.levelInteger == level) {
                    lastLevel.add(lLv);
                }
            }


            for (Nodes nodes : lastLevel) {
                for (Nodes pNodes : lastLevel) {
                    if(nodes.nodeData == pNodes.parentNodeData){
                        indexRepeatLevel=lastLevel.indexOf(pNodes);
                        pNodes.setLevelInteger(null);
                    }
                }
            }


//            งงให้เปิด  ********************
//            for(Node i : lastLevel){
//                System.out.println(i.nodeData +" " +i.parentNodeData +"  "+i.levelInteger);
//            }
//            System.out.println("____________");
//            lastLevel.remove(indexRepeatLevel);
//
//
//            if(level>=10){
//                System.out.println("*****************************************");
//                show();
//               // stop=true;
//                break;
//            }

            //ถ้าไม่เข้า ลูป เช็ค null ก็ จะ หยุด loop while
            level++;
            //ถ้ามี null 1 ตัว ให้ stop = false เพิ่อทำ loop ต่อ
            for (Nodes out : bom) {
                if (out.levelInteger == null) {
                    stop = false;
                    break;
                }
            }

        }

        stop=false;
        while (!stop){
            try {
                stop=true;
                for (int i = 0; i < bom.size(); i++) {
                    countLoop++;
                    if (bom.get(i).nodeData == bom.get(i+1).nodeData) {
                        bom.remove(i);
                        stop = false;
                    }
                }
            }catch (Exception e){
            }
        }

        Collections.sort(bom, new Comparator<Nodes>() {
            @Override
            public int compare(Nodes o1, Nodes o2) {
                return o1.levelInteger.compareTo(o2.levelInteger);
            }
        });
    }

    public void calLevel(){
        int n = -1;
        Integer level = 0;
        for (Nodes i : bom){
            MyCountLoop++;
            n++;
            if(i.parentNodeData == null){//first node
                i.setLevelInt(0);
            }else {
                n--;
                if(i.parentNodeData ==bom.get(n).nodeData){
                    i.setLevelInt(level);
                }
                for(int upDow= 0; upDow<bom.size()-1; upDow++){
                    MyCountLoop++;
                    for(int leftRight = 0 ; leftRight <= bom.size()-1; leftRight++){
                        MyCountLoop++;
                        if(bom.get(upDow).levelInt ==leftRight && i.parentNodeData ==bom.get(n+upDow).nodeData){
                            i.setLevelInt(level+leftRight);
                        }
                    }
                }
            }
        }
    }
    public void showBoom() {
        for (Nodes a : bom) {
            System.out.println(" " + a.nodeData + "  " + a.parentNodeData + " Lv. " + a.levelInt + " ");
        }
        System.out.println(MyCountLoop);
    }
    public void show() {
        for (Nodes a : bom) {
            System.out.println(" " + a.nodeData + "  " + a.parentNodeData + " Lv. " + a.levelInteger + " ");
        }
        System.out.println(countLoop);
    }
}
